use gdnative::api::Area2D;
use gdnative::prelude::*;

/// Get all child cores of the given node, recursively.
pub fn child_cores(parent: &Node) -> Vec<Instance<Core, Shared>> {
    let mut output = Vec::new();
    for child in parent.get_children().iter() {
        if let Some(node) = child.try_to_object::<Node>() {
            let node = unsafe { node.assume_safe() };
            output.extend(child_cores(&node));
            if let Some(child) = node.cast::<<Core as NativeClass>::Base>() {
                if let Some(child) = child.cast_instance::<Core>() {
                    output.push(child.claim());
                }
            }
        }
    }
    output
}

#[derive(NativeClass, Debug, Copy, Clone)]
#[inherit(Area2D)]
pub struct Core {
    #[property(default = 20.0)]
    mass: f32,

    #[property(default = 100.0)]
    radius: f32,
}

impl Default for Core {
    fn default() -> Self {
        Core {
            mass: 20.0,
            radius: 100.0,
        }
    }
}

#[methods]
impl Core {
    fn new(_owner: &Area2D) -> Self {
        Default::default()
    }

    /// Get the acceleration influence of the core on the given global point.
    /// position is the core's position.  This is taken separate so this method doesn't have to
    /// depend on the owner.
    pub fn pull_on_point(&self, position: Vector2, point: Vector2) -> Vector2 {
        let distance_squared = point.distance_squared_to(position) / (32.0 * 32.0);
        let acceleration_direction = point.direction_to(position);
        let acceleration = self.mass * 1000.0 / distance_squared;
        acceleration_direction * acceleration
    }
}
