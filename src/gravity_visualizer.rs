use crate::core::child_cores;
use crate::{Core, GetTypedNode};
use gdnative::api::{Engine, Node2D, Sprite};
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Node2D)]
#[register_with(Self::register)]
pub struct GravityVisualizer {
    #[property(default = 1)]
    radius: u16,

    #[property(default = 32.0)]
    spacing: f32,

    last_radius: u16,

    last_spacing: f32,

    arrow: Option<Ref<Sprite, Unique>>,
}

impl Default for GravityVisualizer {
    fn default() -> Self {
        GravityVisualizer {
            radius: 1,
            spacing: 32.0,
            last_radius: 0,
            last_spacing: 0.0,
            arrow: None,
        }
    }
}

#[methods]
impl GravityVisualizer {
    fn new(_owner: &Node2D) -> Self {
        Default::default()
    }

    #[export]
    fn _ready(&mut self, owner: TRef<Node2D>) {
        // Already been readied
        if matches!(self.arrow, Some(_)) {
            return;
        }

        let node: TRef<Node> = owner.upcast();
        if Engine::godot_singleton().is_editor_hint() {
            // Only do anything if the node is a child of a hole, otherwise it will modify itself
            // in its own scene.
            if let Some(parent) = node.get_parent() {
                let parent = unsafe { parent.assume_safe() };
                let filename = parent.filename().to_string();
                // Note, this is weird.  This is the only way I have found to differentiate between
                // the GravityVisualizer running in its own scene and as an instance of some other
                // scene.  This will fail if the GravityVisualizer isn't a direct child, too.
                // I don't know if this is stable.  A Godot update might break this.
                // Trying to cast as a Hole instance broke this.
                if !filename.trim().is_empty() {
                    // Take exclusive ownership of the arrow
                    let arrow: TRef<Sprite> = unsafe { node.get_typed_node("Arrow") };
                    node.remove_child(arrow);
                    self.arrow = Some(unsafe { arrow.assume_unique() });
                }
            }
        } else {
            // Instantly remove itself if not in the editor.
            node.queue_free();
        }
    }

    fn reset_arrows(&mut self, owner: TRef<Node2D>) {
        // First remove all existing children
        for child in owner.get_children().into_iter() {
            if let Some(child) = child.try_to_object::<Sprite>() {
                owner.remove_child(child);
                let child = unsafe { child.assume_unique() };
                child.queue_free();
            }
        }

        let radius = self.radius as i32;

        for y in -radius..=radius {
            for x in -radius..=radius {
                let new_arrow = self.arrow.as_ref().unwrap().duplicate(15).unwrap();
                let new_arrow = unsafe { new_arrow.assume_unique() };
                let new_arrow: Ref<Sprite, Unique> = new_arrow.cast().unwrap();
                new_arrow.set_position(Vector2::new(
                    x as f32 * self.spacing,
                    y as f32 * self.spacing,
                ));
                owner.add_child(new_arrow, false);
            }
        }

        self.last_spacing = self.spacing;
        self.last_radius = self.radius;
    }

    #[export]
    fn _process(&mut self, owner: TRef<Node2D>, _delta: f32) {
        if let Some(_) = self.arrow.as_ref() {
            if (self.last_radius, self.last_spacing) != (self.radius, self.spacing) {
                self.reset_arrows(owner);
            }
            let parent = unsafe { owner.get_parent().unwrap().assume_safe() };
            let cores: Vec<(Core, Vector2)> = child_cores(&parent)
                .into_iter()
                .map(|core| {
                    let core = unsafe { core.assume_safe() };
                    core.map(|script, base| (*script, base.global_position()))
                        .unwrap()
                })
                .collect();

            for child in owner.get_children().into_iter() {
                if let Some(child) = child.try_to_object::<Sprite>() {
                    let child = unsafe { child.assume_safe() };
                    let position = child.global_position();
                    let acceleration =
                        cores
                            .iter()
                            .fold(Vector2::zero(), |acc, (core, core_position)| {
                                acc + core.pull_on_point(*core_position, position)
                            });
                    if acceleration != Vector2::zero() {
                        child.set_rotation(acceleration.angle_from_x_axis().to_f64().get());
                    }
                }
            }
        }
    }
}
