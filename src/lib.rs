use gdnative::prelude::*;

mod controller_spinbox;
mod core;
mod course;
mod course_loader;
mod game;
mod game_timer;
mod get_typed_node;
mod gravity_visualizer;
mod gravpack_shot_overlay;
mod hole;
mod main_menu;
mod music_shuffle_player;
mod orphan_watchers;
mod pause_menu;
mod paused_emitter;
mod player;
mod settings;
mod settings_menu;

use crate::controller_spinbox::ControllerSpinbox;
use crate::core::Core;
use crate::course::Course;
use crate::course_loader::CourseLoader;
use crate::game::Game;
use crate::game_timer::GameTimer;
use crate::get_typed_node::GetTypedNode;
use crate::gravity_visualizer::GravityVisualizer;
use crate::gravpack_shot_overlay::GravpackShotOverlay;
use crate::hole::Hole;
use crate::main_menu::MainMenu;
use crate::music_shuffle_player::MusicShufflePlayer;
use crate::orphan_watchers::OrphanInstanceWatcher;
use crate::pause_menu::PauseMenu;
use crate::paused_emitter::PausedEmitter;
use crate::player::Player;
use crate::settings::Settings;
use crate::settings_menu::SettingsMenu;

fn init(handle: InitHandle) {
    handle.add_class::<ControllerSpinbox>();
    // Core needs to be a tool class to enable GravityVisualizer to access its non-default
    // attributes
    handle.add_tool_class::<Core>();
    handle.add_class::<Course>();
    handle.add_class::<CourseLoader>();
    handle.add_class::<Game>();
    handle.add_tool_class::<GravityVisualizer>();
    handle.add_class::<GravpackShotOverlay>();
    handle.add_class::<Hole>();
    handle.add_class::<MainMenu>();
    handle.add_class::<MusicShufflePlayer>();
    handle.add_class::<PauseMenu>();
    handle.add_class::<PausedEmitter>();
    handle.add_class::<Player>();
    handle.add_class::<SettingsMenu>();
}

godot_init!(init);
