use crate::{CourseLoader, GetTypedNode, Settings, SettingsMenu};
use gdnative::api::{Button, CanvasLayer, Panel, Resource, VBoxContainer};
use gdnative::prelude::*;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(NativeClass, Default, Debug)]
#[inherit(CanvasLayer)]
#[register_with(Self::register)]
pub struct MainMenu {
    #[property]
    course_loaders: VariantArray,

    choose_course_panel: Option<Ref<Panel>>,
    choose_course_button: Option<Ref<Button>>,
    choose_course_container: Option<Ref<VBoxContainer>>,
    choose_course_back_button: Option<Ref<Button>>,
    settings_menu: Option<Instance<SettingsMenu, Shared>>,
    settings: Rc<RefCell<Settings>>,
}

#[methods]
impl MainMenu {
    fn register(builder: &ClassBuilder<Self>) {
        builder.add_signal(Signal {
            name: "exit",
            args: &[],
        });
        builder.add_signal(Signal {
            name: "start_course",
            args: &[SignalArgument {
                name: "course",
                default: Variant::from_object(Null::<Object>::null()),
                export_info: ExportInfo::new(VariantType::Object),
                usage: PropertyUsage::DEFAULT,
            }],
        });
    }

    fn new(_owner: &CanvasLayer) -> Self {
        Default::default()
    }

    pub fn set_settings(&mut self, settings: Rc<RefCell<Settings>>) {
        self.settings = settings.clone();
        if let Some(settings_menu) = self.settings_menu.as_ref() {
            let settings_menu = unsafe { settings_menu.assume_safe() };
            settings_menu
                .map_mut(|script, _| script.set_settings(settings))
                .unwrap();
        }
    }

    #[export]
    fn _ready(&mut self, owner: TRef<CanvasLayer>) {
        let node: TRef<Node> = owner.upcast();

        self.choose_course_button =
            Some(unsafe { node.get_claimed_node("Panel/VBoxContainer/ChooseCourse") });
        self.choose_course_panel = Some(unsafe { node.get_claimed_node("ChooseCourse") });
        self.choose_course_back_button =
            Some(unsafe { node.get_claimed_node("ChooseCourse/VBoxContainer/Back") });

        let settings_menu: RefInstance<SettingsMenu, _> =
            unsafe { node.get_typed_instance("SettingsMenu") };
        settings_menu
            .map_mut(|script, _| script.set_settings(self.settings.clone()))
            .unwrap();
        self.settings_menu = Some(settings_menu.claim());

        let choose_course_container: TRef<VBoxContainer, _> =
            unsafe { node.get_typed_node("ChooseCourse/VBoxContainer") };
        for course_loader in self.course_loaders.iter() {
            let course_loader: Ref<Resource> = course_loader.try_to_object().unwrap();
            let course_loader = unsafe { course_loader.assume_safe() };
            let course_loader = course_loader.cast_instance::<CourseLoader>().unwrap();

            let course_button = Button::new();
            course_loader
                .map(|loader, _| course_button.set_text(loader.course_name()))
                .unwrap();
            let course_loader = course_loader.claim();
            let button_font =
                ResourceLoader::godot_singleton().load("res://ButtonFont.tres", "", false);
            course_button.set("custom_fonts/font", button_font);
            let course_button = course_button.into_shared();
            choose_course_container.add_child(course_button, false);
            let course_chosen_array = VariantArray::new();
            course_chosen_array.push(course_loader);
            let course_button = unsafe { course_button.assume_safe() };
            course_button
                .connect(
                    "pressed",
                    owner,
                    "start_course",
                    course_chosen_array.into_shared(),
                    0,
                )
                .unwrap();
        }
        self.choose_course_container = Some(choose_course_container.claim());

        self.setup(owner);
    }

    #[export]
    pub fn setup(&self, _owner: TRef<CanvasLayer>) {
        let choose_course_panel = unsafe { self.choose_course_panel.unwrap().assume_safe() };
        let settings_menu = unsafe { self.settings_menu.as_ref().unwrap().assume_safe() };
        let choose_course_button = unsafe { self.choose_course_button.unwrap().assume_safe() };
        choose_course_panel.set_visible(false);
        settings_menu.base().set_visible(false);
        choose_course_button.grab_focus();
    }

    #[export]
    fn exit(&self, owner: TRef<CanvasLayer>) {
        owner.emit_signal("exit", &[]);
    }

    #[export]
    fn show_courses(&self, _owner: TRef<CanvasLayer>) {
        let choose_course_panel = unsafe { self.choose_course_panel.unwrap().assume_safe() };
        let back_button = unsafe { self.choose_course_back_button.unwrap().assume_safe() };
        choose_course_panel.set_visible(true);
        back_button.grab_focus();
    }

    #[export]
    fn show_settings(&self, _owner: TRef<CanvasLayer>) {
        let settings_menu = unsafe { self.settings_menu.as_ref().unwrap().assume_safe() };
        settings_menu
            .map(|script, base| script.show(&base))
            .unwrap();
    }

    #[export]
    fn start_course(&self, owner: TRef<CanvasLayer>, course_loader: Ref<Resource>) {
        owner.emit_signal("start_course", &[Variant::from_object(course_loader)]);
    }
}
